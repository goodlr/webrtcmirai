// https://www.webrtc-experiment.com/

var fs = require("fs");

// don't forget to use your own keys!
// var options = {
  // key: fs.readFileSync("fake-keys/privatekey.pem"),
  // cert: fs.readFileSync("fake-keys/certificate.pem"),
  // key: fs.readFileSync('/etc/letsencrypt/live/webrtcweb.com/privkey.pem'),
  // cert: fs.readFileSync('/etc/letsencrypt/live/webrtcweb.com/fullchain.pem')
// };

const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};

// HTTPs server
var app = require("https").createServer(options, function (request, response) {
  response.writeHead(200, {
    "Content-Type": "text/html",
  });
  //   var link =
  //     "https://github.com/muaz-khan/WebRTC-Experiment/tree/master/socketio-over-nodejs";
  response.write("<title>TGL Solutions</title><h1><a>Video Broadcast</a>");
  response.end();
});

// socket.io goes below

var io = require("socket.io").listen(app, {
  log: true,
  origins: "*:*",
});

io.set("transports", [
  // 'websocket',
  "xhr-polling",
  "jsonp-polling",
]);

var channels = {};

io.sockets.on("connection", function (socket) {
  var initiatorChannel = "";
  if (!io.isConnected) {
    io.isConnected = true;
  }

  socket.on("new-channel", function (data) {
    console.log("new-channel", data);
    if (!channels[data.channel]) {
      initiatorChannel = data.channel;
    }

    channels[data.channel] = data.channel;
    onNewNamespace(data.channel, data.sender);
  });

  socket.on("presence", function (channel) {
    console.log("presence", channel);
    var isChannelPresent = !!channels[channel];
    socket.emit("presence", isChannelPresent);
  });

  socket.on("disconnect", function (channel) {
    if (initiatorChannel) {
      delete channels[initiatorChannel];
    }
  });
});

function onNewNamespace(channel, sender) {
  io.of("/" + channel).on("connection", function (socket) {
    var username;
    if (io.isConnected) {
      io.isConnected = false;
      socket.emit("connect", true);
    }

    socket.on("message", function (data) {
      console.log("message", data);
      if (data.sender == sender) {
        if (!username) username = data.data.sender;

        socket.broadcast.emit("message", data.data);
      }
    });

    socket.on("disconnect", function () {
      console.log("disconnect", username);
      if (username) {
        socket.broadcast.emit("user-left", username);
        username = null;
      }
    });
  });
}

// run app

var server = app.listen(process.env.PORT || 5000, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('running at http://' + host + ':' + port)
});

process.on("unhandledRejection", (reason, promise) => {
  process.exit(1);
});

// console.log(
//   "Please open URL: " + app.address().address + (process.env.PORT || 3000) + "/"
// );
